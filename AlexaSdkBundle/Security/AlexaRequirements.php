<?php
/**
 * Created by PhpStorm.
 * User: Alessandro Battaglia
 * Date: 18/12/2018
 * Time: 21:56
 */

namespace App\Ba\AlexaSdkBundle\Security;


use Symfony\Component\HttpFoundation\Request;

class AlexaRequirements
{
    /**
     * @param $request
     *
     * @return array
     */
    public static function check($request)
    {
        $certificateUrl = $request->headers->get('signaturecertchainurl');

        $logData = [
            'certificate_url' => $certificateUrl,
            'state' => [
                'state' => true
            ],
        ];

        if (!isset($certificateUrl) || $certificateUrl == '') {
            $logData['state'] = [
                'state' => false,
                'message' => 'No certificate url',
            ];
        } else {
            $validateUri = self::validateUri($certificateUrl);
            $validateCertificate = self::validateCertificate($request);

            if (!$validateUri['state']) {
                $logData['state'] = $validateUri;
            } elseif (!$validateCertificate['state']) {
                $logData['state'] = $validateCertificate;
            }
        }

        self::log(JSON_encode($logData));

        return $logData;
    }

    private static function validateUri($uri)
    {
        $uriParts = parse_url($uri);

        if (strcasecmp($uriParts['host'], 's3.amazonaws.com') != 0) {
            return [
                'state' => false,
                'message' => 'The host for the Certificate provided in the header is invalid',
            ];
        }

        self::log('done 1');

        if (strpos($uriParts['path'], '/echo.api/') !== 0) {
            return [
                'state' => false,
                'message' => 'The host for the Certificate provided in the header is invalid',
            ];
        }

        self::log('done 2');

        if (strcasecmp($uriParts['scheme'], 'https') != 0) {
            return [
                'state' => false,
                'message' => 'The URL is using an unsupported scheme. Should be https',
            ];
        }

        self::log('done 3');

        if (array_key_exists('port', $uriParts) && $uriParts['port'] != '443') {
            return [
                'state' => false,
                'message' => 'The URL is using an unsupported https port',
            ];
        }

        self::log('done 4');

        return [
            'state' => true,
        ];
    }

    private static function validateCertificate(Request $request)
    {
        $signature = $request->headers->get('signature');
        $url = $request->headers->get('signaturecertchainurl');

        $md5pem = __DIR__ . '/../../../../var/' . md5($url) . '.pem';
        $echoDomain = 'echo-api.amazon.com';

        // If we haven't received a certificate with this URL before,
        // store it as a cached copy
        if (!file_exists($md5pem)) file_put_contents($md5pem, file_get_contents($url));

        self::log('done 5');

        // Validate certificate chain and signature
        $pem = file_get_contents($md5pem);
        $ssl_check = openssl_verify($request->getContent(), base64_decode($signature), $pem, 'sha1');
        if ($ssl_check != 1) {
            return [
                'state' => false,
                'message' => openssl_error_string(),
            ];
        }

        self::log('done 6');

        // Parse certificate for validations below
        $parsedCertificate = openssl_x509_parse($pem);
        if (!$parsedCertificate) {
            return [
                'state' => false,
                'message' => 'x509 parsing failed',
            ];
        }

        self::log('done 7');

        // Check that the domain echo-api.amazon.com is present in
        // the Subject Alternative Names (SANs) section of the signing certificate
        if (strpos($parsedCertificate['extensions']['subjectAltName'], $echoDomain) === false) {
            return [
                'state' => false,
                'message' => 'subjectAltName Check Failed',
            ];
        }

        self::log('done 8');

        // Check that the signing certificate has not expired
        // (examine both the Not Before and Not After dates)
        $validFrom = $parsedCertificate['validFrom_time_t'];
        $validTo = $parsedCertificate['validTo_time_t'];
        $time = time();
        if (!($validFrom <= $time && $time <= $validTo)) {
            return [
                'state' => false,
                'message' => 'certificate expiration check failed',
            ];
        }

        self::log('done 9');

        return [
            'state' => true,
        ];
    }

    private static function log($message)
    {
        if(false) { // TODO: Find a logic
            file_put_contents(__DIR__ . '/log_' . date("j.n.Y") . '.log', $message . "\n", FILE_APPEND);
        }
    }
}