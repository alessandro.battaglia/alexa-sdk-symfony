<?php
/**
 * Created by PhpStorm.
 * User: Alessandro Battaglia
 * Date: 18/12/2018
 * Time: 21:47
 */

namespace App\Ba\AlexaSdkBundle\Security;


use App\Ba\AlexaSdkBundle\Utility\Response\AlexaResponse;
use App\Ba\AlexaSdkBundle\Utility\Request\Content;
use App\Ba\AlexaSdkBundle\Utility\Response\Card;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;

class AlexaAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface
{
    private $isLocal = false;

    public function createToken(Request $request, $providerKey)
    {
        $this->isLocal = preg_match('/^(http(s)?(:\/\/))?(www\.)?' . str_replace('.', '\.', $request->getHost()) . '\:' . $request->getPort() . '(\/.*)?$/', $request->headers->get('referer'));

        // Check if the url is from Amazon
        $result = AlexaRequirements::check($request);
        if (!$result['state']['state']) {
            throw new AuthenticationException('Invalid certificate url: ' . $result['state']['message'], 400);
        }

        // Handle request
        $content = Content::handleRequest($request);

        return new PreAuthenticatedToken(
            'anon.',
            $content->getSession()->getUserId(),
            $providerKey
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        if (!$userProvider instanceof AlexaProvider) {
            throw new \InvalidArgumentException(
                sprintf(
                    'The user provider must be an instance of AlexaProvider (%s was given).',
                    get_class($userProvider)
                )
            );
        }

        /*
        $user = $userProvider->getUserFromApiKey($apiKey, $this->isLocal);

        if (!$user) {
            // CAUTION: this message will be returned to the client
            // (so don't put any un-trusted messages / error strings here)
            throw new CustomUserMessageAuthenticationException(
                JSON_encode(['message' => sprintf('API Key "%s" does not exist.', $apiKey)])
            );
        }
        */

        /*
        $username = $userProvider->getUsernameForApiKey($apiKey);

        if (!$username) {
            // CAUTION: this message will be returned to the client
            // (so don't put any un-trusted messages / error strings here)
            throw new CustomUserMessageAuthenticationException(
                sprintf('API Key "%s" does not exist.', $apiKey)
            );
        }
        */

        $userId = $token->getCredentials();

        $user = $userProvider->loadUserByUsername($userId);

        return new PreAuthenticatedToken(
            $user,
            $userId,
            $providerKey,
            array_merge($user->getRoles(), ['ROLE_ALEXA'])
        );
    }

    /**
     * This is called when an interactive authentication attempt fails. This is
     * called by authentication listeners inheriting from
     * AbstractAuthenticationListener.
     *
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return Response The response to return, never null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // this contains information about *why* authentication failed

        return AlexaResponse::createResponse(new Card($exception->getMessage(), 'An error occurred'), null, $exception->getCode());
    }
}