<?php
/**
 * Created by PhpStorm.
 * User: Alessandro Battaglia
 * Date: 18/12/2018
 * Time: 15:51
 */

namespace App\Ba\AlexaSdkBundle\Utility\Request;


use Symfony\Component\Security\Core\User\UserInterface;

class Session
{
    /**
     * @var array
     */
    private $session;

    /**
     * @var string
     */
    private $userId;

    /**
     * @param Content $content
     *
     * @return $this
     */
    public function handleContent(Content $content)
    {
        $this->session = $content->getContent()['session'];
        $this->userId = $this->session['user']['userId'];

        return $this;
    }

    /** Custom Getters and Setters */

    /**
     * @return boolean
     */
    public function isNew()
    {
        return $this->session['new'];
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->session['id'];
    }

    /**
     * @return array
     */
    public function getApplication()
    {
        return $this->session['application'];
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }
}