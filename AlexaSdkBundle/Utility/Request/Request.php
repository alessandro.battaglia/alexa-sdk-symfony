<?php
/**
 * Created by PhpStorm.
 * User: Alessandro Battaglia
 * Date: 18/12/2018
 * Time: 16:49
 */

namespace App\Ba\AlexaSdkBundle\Utility\Request;

class Request
{
    /**
     * @var array
     */
    private $request;

    public function handleContent(Content $content)
    {
        $this->request = $content->getContent()['request'];

        return $this;
    }

    /** Getters and Setters */

    /**
     * @return array
     */
    public function getRequest(): array
    {
        return $this->request;
    }

    /** Custom Getters and Setters */

    public function getType()
    {
        return $this->request['type'];
    }

    public function getId()
    {
        return $this->request['requestId'];
    }

    public function getTimestamp()
    {
        return $this->request['timestamp'];
    }

    public function getLocale()
    {
        return $this->request['locale'];
    }

    public function shouldLinkResultBeReturned()
    {
        return $this->request['shouldLinkResultBeReturned'];
    }
}