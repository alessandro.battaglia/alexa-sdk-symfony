<?php
/**
 * Created by PhpStorm.
 * User: Alessandro Battaglia
 * Date: 18/12/2018
 * Time: 15:36
 */

namespace App\Ba\AlexaSdkBundle\Utility\Request;


use Symfony\Component\HttpFoundation\Request as HttpRequest;

class Content
{
    /**
     * @var HttpRequest
     */
    private $httpRequest;

    /**
     * @var array
     */
    private $content;

    /**
     * @var Session
     */
    private $session;

    private $request;

    /**
     * @param HttpRequest $httpRequest
     *
     * @return Content
     */
    public static function handleRequest(HttpRequest $httpRequest)
    {
        $content = new self();

        $content->httpRequest = $httpRequest;
        $content->content = JSON_decode($httpRequest->getContent(), true);

        $content->session = new Session();
        $content->session->handleContent($content);

        // TODO: Implement context

        $content->request = new Request();
        $content->request->handleContent($content);

        return $content;
    }

    /** Getters and Setters */

    /**
     * @return HttpRequest
     */
    public function getHttpRequest(): HttpRequest
    {
        return $this->httpRequest;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @return Session
     */
    public function getSession(): Session
    {
        return $this->session;
    }

    /** Custom Getters and Setters */

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->content['version'];
    }
}