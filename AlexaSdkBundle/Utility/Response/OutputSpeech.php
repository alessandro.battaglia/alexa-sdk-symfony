<?php
/**
 * Created by PhpStorm.
 * User: Alessandro Battaglia
 * Date: 19/12/2018
 * Time: 12:26
 */

namespace App\Ba\AlexaSdkBundle\Utility\Response;


class OutputSpeech
{
    const TYPE_PLAIN_TEXT = "PlainText";

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $text;

    /**
     * OutputSpeech constructor.
     * @param string $text
     * @param string $type
     */
    public function __construct(string $text, string $type = self::TYPE_PLAIN_TEXT)
    {
        $this->text = $text;
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function __toArray()
    {
        return [
            "type" => $this->getType(),
            "text" => $this->getText(),
        ];
    }

    /** Getters and Setters */

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return OutputSpeech
     */
    public function setType(string $type): OutputSpeech
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return OutputSpeech
     */
    public function setText(string $text): OutputSpeech
    {
        $this->text = $text;
        return $this;
    }
}