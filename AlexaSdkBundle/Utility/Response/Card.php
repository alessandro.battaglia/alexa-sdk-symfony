<?php
/**
 * Created by PhpStorm.
 * User: Alessandro Battaglia
 * Date: 19/12/2018
 * Time: 12:26
 */

namespace App\Ba\AlexaSdkBundle\Utility\Response;


class Card
{
    const TYPE_SIMPLE = "Simple";

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $type;

    /**
     * Card constructor.
     * @param string $content
     * @param string|null $title
     * @param string $type
     */
    public function __construct(string $content, string $title = null, string $type = self::TYPE_SIMPLE)
    {
        $this->content = $content;
        $this->title = $title;
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function __toArray()
    {
        return [
            "type" => $this->getType(),
            "title" => $this->getTitle(),
            "content" => $this->getContent(),
        ];
    }

    /** Getters and Setters */

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Card
     */
    public function setContent(string $content): Card
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Card
     */
    public function setTitle(string $title): Card
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Card
     */
    public function setType(string $type): Card
    {
        $this->type = $type;
        return $this;
    }
}