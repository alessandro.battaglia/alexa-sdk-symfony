<?php
/**
 * Created by PhpStorm.
 * User: Alessandro Battaglia
 * Date: 18/12/2018
 * Time: 15:34
 */

namespace App\Ba\AlexaSdkBundle\Utility\Response;


use Symfony\Component\HttpFoundation\JsonResponse;

class AlexaResponse extends JsonResponse
{
    /**
     * @var OutputSpeech
     */
    protected $outputSpeech;

    /**
     * @var Card
     */
    protected $card;

    /**
     * @var AlexaResponse
     */
    protected $reprompt; // TODO: Do it

    /*
     * $reprompt ? [
     *      "outputSpeech" => [
     *          "type" => "PlainText",
     *          "text" => $content,
     *      ],
     *  ]
     */

    /**
     * @var bool
     */
    protected $shouldEndSession = true;

    /**
     * AlexaResponse constructor.
     * @param string|null $message
     * @param int $status
     * @param array $headers
     */
    public function __construct(string $message = null, int $status = 200, array $headers = array())
    {
        parent::__construct([], $status, $headers);

        $this->setCard(null)->setOutputSpeech(null);
    }

    public static function createResponse(Card $card = null, OutputSpeech $outputSpeech = null, $status = 200, $headers = array())
    {
        $response = new static(null, $status, $headers);

        $response->setCard($card)->setOutputSpeech($outputSpeech);

        return $response;
    }

    /**
     * @return AlexaResponse
     */
    protected function updateData()
    {
        return $this->setData([
            "version" => "1.0",
            "sessionAttributes" => [
                "supportedHoroscopePeriods" => [
                    "daily" => true,
                    "weekly" => false,
                    "monthly" => false,
                ]
            ],
            "response" => [
                "outputSpeech" => $this->getOutputSpeech(),
                "card" => $this->getCard(),
                "reprompt" => null,
                "shouldEndSession" => $this->shouldEndSession,
            ],
        ]);
    }

    /** Getters and Setters */

    /**
     * @param bool $instance
     * @return null|OutputSpeech|array
     */
    public function getOutputSpeech(bool $instance = false)
    {
        return $instance || is_null($this->outputSpeech) ? $this->outputSpeech : $this->outputSpeech->__toArray();
    }

    /**
     * @param null|OutputSpeech $outputSpeech
     * @return AlexaResponse
     */
    public function setOutputSpeech(?OutputSpeech $outputSpeech): AlexaResponse
    {
        $this->outputSpeech = $outputSpeech;

        $this->updateData();

        return $this;
    }

    /**
     * @param bool $instance
     * @return null|Card|array
     */
    public function getCard(bool $instance = false)
    {
        return $instance || is_null($this->card) ? $this->card : $this->card->__toArray();
    }

    /**
     * @param null|Card $card
     * @return AlexaResponse
     */
    public function setCard(?Card $card): AlexaResponse
    {
        $this->card = $card;

        $this->updateData();

        return $this;
    }

    /**
     * @return bool
     */
    public function isShouldEndSession(): bool
    {
        return $this->shouldEndSession;
    }

    /**
     * @param bool $shouldEndSession
     * @return AlexaResponse
     */
    public function setShouldEndSession(bool $shouldEndSession): AlexaResponse
    {
        $this->shouldEndSession = $shouldEndSession;

        $this->updateData();

        return $this;
    }
}