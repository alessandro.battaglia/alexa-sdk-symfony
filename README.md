# Alexa SDK - Symfony

Alexa SDK for Symfony

## How to use



### Install

Clone repo in *src/Ba* directory:

```sh
git clone https://gitlab.com/alessandro.battaglia/alexa-sdk-symfony.git AlexaSdkBundle
```

Install the bundle in *config/boundles.php*:


```sh
return [
    [...],
    App\Ba\AlexaSdkBundle\BaAlexaSdkBundle::class => ['all' => true],
]
```

### Initialize the project:

Add "ext-json" and "ext-openssl" in *composer.json* file:

```sh
{
  [...]
  "require": {
    [...]
    "ext-json": "*",
    "ext-openssl": "*"
  },
  [...]
}
```

---

Install the security bundle:

```sh
composer require symfony/security-bundle
```

---

Configure provider:

Why custom provider?
Amazon servers check the response of the skill sending fake requests to the skill Endpoint.
The custom provider check if the request agrees the Amazon directives.

```yaml
# config/packages/security.yaml
security:
    providers:

        [...]

        alexa_provider:
            id: App\Ba\AlexaSdkBundle\Security\AlexaProvider

    [...]

    firewalls:
        alexa:
            pattern: /
            stateless: true
            simple_preauth:
                authenticator: App\Ba\AlexaSdkBundle\Security\AlexaAuthenticator
            provider: alexa_provider

    [...]
```

